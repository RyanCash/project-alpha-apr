from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm
from django.urls import reverse


# Create your views here.


@login_required
def list_project(request):
    project = Project.objects.filter(owner=request.user)
    context = {"projects": project}
    return render(request, "projects/projects_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    context = {"project": project, "tasks": tasks}
    return render(request, "project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
        context = {"form": form}
    return render(request, "create_projects.html", context)


@login_required
def project_detail(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    create_task_url = reverse("tasks:create_task", args=[project_id])
    context = {"project": project, "create_task_url": create_task_url}
    return render(request, "project_detail.html", context)
