from django.urls import path
from tasks.views import create_tasks, show_my_tasks

app_name = "tasks"
urlpatterns = [
    path("create/", create_tasks, name="create_tasks"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
