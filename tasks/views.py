from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskCreateForm
from .models import Task

# Create your views here.
@login_required
def create_tasks(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = TaskCreateForm()
        context = {"form": form}
    return render(request, "create_tasks.html", context)


@login_required
def show_my_tasks(request):
    if request.method == "GET":
        tasks = Task.objects.filter(assignee=request.user)
        context = {"tasks": tasks}
    return render(request, "show_my_tasks.html", context)
